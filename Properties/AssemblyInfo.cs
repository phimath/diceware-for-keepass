﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DicewareForKeePass")]
[assembly: AssemblyDescription("Lets you generate passwords with the Diceware technique.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("phimath")]
[assembly: AssemblyProduct("KeePass Plugin")]
[assembly: AssemblyCopyright("Copyright (c) 2021 phimath")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("1deefbb1-2180-4395-9d4a-1f16bc382a05")]

[assembly: AssemblyVersion("1.2.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]
