# DicewareForKeePass - Generate Passphrases
A KeePass passphrase generator based on the principal as found in the [Diceware Wikipedia Article](https://en.wikipedia.org/wiki/Diceware).

The original author Heinrich Ulbricht created this plugin as a tribute to https://xkcd.com/936/. His source can be found on [Github](https://github.com/heinrich-ulbricht/wuerfelware-passphrases-for-keepass/).


# How does Diceware work?
The plugin registers as a password generator in KeePass and generates passphrases. Passphrases are passwords consisting of multiple words, thus they are "phrases".

The words used to make passphrases are taken from a text file. Words are randomly chosen. The number of words per phrase can be configured. 

The resulting passphrases look like those:

* dobbs bella bump flash begin ansi
* beraet fauna schere sampan herauf
* easel venom aver flung jon call

These phrases are hard to guess but easy to memorize and/or type.

# Installation
Download the latest release [here](https://gitlab.com/phimath/diceware-for-keepass/-/releases). 

Then:
1. Create a (new) sub-folder of the KeePass plugins folder (e.g. `KeePass\Plugins\diceware`) if it does not already exist
1. Unzip the files from the release to that folder

# Changing the word list
Only one word list is supported - it's in the *wuerfelware.txt* file. You can change the content of this file as you wish.

Whitespaces and line breaks are used as word delimiters, so a text file containing *correct battery horse staple* will be read as a word list containing 4 words.

Any numbers (like *12435*) in this file will be ignored which is handy if you use an existing word list (like the initial *wuerfelware.txt*). Just leave the numbers in, they do no harm.

# Supported Languages
The plugin language is English.

The initial word list *wuerfelware.txt* contains German words. You can replace the file content with words in any language.

# Word lists
The initial word list *wuerfelware.txt* was downloaded from here:
http://world.std.com/~reinhold/diceware_german.txt (*A German word list provided by Benjamin Tenne under the terms of the GNU General Public License.*)

I did not alter it, just renamed it to make the file name more generic.

There are more files in different languages available, you can get an overview here:
http://world.std.com/~reinhold/diceware.html

Many more word lists can be found. A good keyword to google for is *Diceware*.
