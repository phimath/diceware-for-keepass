﻿using System.Windows.Forms;

namespace Phimath.KeePass.Diceware
{
    public partial class ConfigurationForm : Form
    {
        private readonly Config _configuration;

        public ConfigurationForm(Config configuration, string filePath, int wordCount)
        {
            _configuration = configuration;

            InitializeComponent();

            nudWords.Minimum = Config.MinWords;
            nudWords.Maximum = Config.MaxWords;

            tbFilePath.Text = filePath;
            lblNumberOfWords.Text = wordCount.ToString();
            nudWords.Value = _configuration.WordCount;
            tbSeparator.Text = _configuration.Separator;
            cbCapitalization.Checked = _configuration.Capitalization;
        }

        public Config ShowDialogWithResult()
        {
            var result = ShowDialog();

            _configuration.WordCount = decimal.ToInt32(nudWords.Value);
            _configuration.Separator = tbSeparator.Text;
            _configuration.Capitalization = cbCapitalization.Checked;

            return _configuration;
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}