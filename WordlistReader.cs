﻿/*
    Copyright (c) 2020 phimath 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Phimath.KeePass.Diceware
{
    /// <summary>
    /// Read words from a file.
    /// </summary>
    /// <remarks>
    /// Reads words from a text file. The text file can contain any list of words or text. Pure Numbers (like "123", "ab12cd" is fine) will be ignored.
    /// </remarks>
    public class WordlistReader
    {
        private static readonly Regex NumberFilterRegex = new Regex(@"^(?>\d*)?\s*(.*?)\s*$", RegexOptions.Compiled);

        private List<string> _entries;
        public string FilePath { get; }

        public WordlistReader(string filePath)
        {
            FilePath = filePath;
        }

        public void LoadList()
        {
            if (!File.Exists(FilePath))
            {
                throw new FileNotFoundException("Cannot find word file at '" + FilePath +
                                                "'. Maybe you forgot copying it to the plugin directory?");
            }

            var content = File.ReadAllText(FilePath);
            // get every word, split at certain whitespace characters
            var entries = content.Split(new[] {Environment.NewLine, "\n", "\r"}, StringSplitOptions.RemoveEmptyEntries);
            _entries = new List<string>(entries.Length);
            foreach (var candidate in entries)
            {
                // ignore numbers
                _entries.Add(NumberFilterRegex.Match(candidate).Groups[1].Value);
            }
        }

        public string GetEntry(int index)
        {
            return _entries[index];
        }

        public int MaxValidIndex => _entries.Count - 1;
        public int EntryCount => _entries.Count;
    }
}