﻿/*
    Copyright (c) 2020 phimath 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


using Newtonsoft.Json;

namespace Phimath.KeePass.Diceware
{
    public class Config
    {
        public const int DefaultWordCount = 6;
        public const int MinWords = 1;
        public const int MaxWords = 100;
        public const string DicewareFilename = "wuerfelware.txt";

        /// <summary>
        /// Number of words to use for a single pass phrase
        /// </summary>
        public int WordCount { get; set; }

        public bool Capitalization { get; set; }

        public string Separator { get; set; }

        private Config()
        {
            WordCount = DefaultWordCount;
            Capitalization = false;
            Separator = "-";
        }

        public static Config FromCurrentOptionsOrNew(string strCurrentOptions)
        {
            return string.IsNullOrEmpty(strCurrentOptions)
                ? new Config()
                : JsonConvert.DeserializeObject<Config>(strCurrentOptions);
        }
    }
}