﻿/*
    Copyright (c) 2020 phimath 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using KeePass.Plugins;

// ReSharper disable once CheckNamespace
namespace Phimath.KeePass.Diceware.Phimath.KeePass
{
    public sealed class DicewareExt : Plugin
    {
        private const string ConstUpdateUrl = "https://api.phimath.de/staticserve/keepass-plugins/VERSIONS.txt";
        
        private IPluginHost _pluginHost = null;
        private DicewareGenerator _dicewareGenerator = null;

        public override string UpdateUrl => ConstUpdateUrl;

        public override bool Initialize(IPluginHost host)
        {
            if (host == null)
            {
                return false;
            }

            _pluginHost = host;
            _dicewareGenerator = new DicewareGenerator();
            _pluginHost.PwGeneratorPool.Add(_dicewareGenerator);

            return true;
        }

        public override void Terminate()
        {
            if (_pluginHost != null)
            {
                _pluginHost.PwGeneratorPool.Remove(_dicewareGenerator.Uuid);
                _dicewareGenerator = null;
                _pluginHost = null;
            }
        }
    }
}
